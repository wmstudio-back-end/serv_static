function initialize(callback){
    var PROTO_PATH = global.dirRoot + '/proto/worker_service.proto';
    var grpc = require('grpc');
    var _proto = grpc.load(PROTO_PATH).worker_service;
    global.Server.grpc = new _proto.ServiceWorker('192.168.102.231:50052',
        grpc.credentials.createInsecure());
    callback(null, 'client_controllers INIT')
}

exports.initialize = initialize;