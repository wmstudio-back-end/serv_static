var cookie       = require('cookie');
var cookieParser = require('cookie-parser');
var OPTIONS      = false;
var https        = require('https');
var http         = require('http');
var fs           = require('fs');
var path         = require('path');
var qs           = require('querystring');
var request      = require('request');
const crypto     = require('crypto');
// process.umask(0)

module.exports.initialize = initialize;

function initialize() {
    console.log('------');
    var PROTO_PATH = global.dirRoot + '/proto/worker_service.proto';
    var grpc       = require('grpc');
    var _proto     = grpc.load(PROTO_PATH).worker_service;
    console.log("workerHOST_port", global.config.serv_worker_host + ":" + global.config.serv_worker_port);
    global.Server.grpc = new _proto.ServiceWorker(global.config.serv_worker_host + ":" + global.config.serv_worker_port, grpc.credentials.createInsecure());
    if (global.argv.hasOwnProperty('https')) {
        var cert       = '/etc/nginx/ssl/ssl-bundle.crt';
        var privateKey = '/etc/nginx/ssl/private.pem';

        if (fs.existsSync('/etc/letsencrypt/live/static.wms-dev.xyz/fullchain.pem')) {
            cert = '/etc/letsencrypt/live/static.wms-dev.xyz/fullchain.pem';
        }

        if (fs.existsSync('/etc/letsencrypt/live/static.wms-dev.xyz/privkey.pem')) {
            privateKey = '/etc/letsencrypt/live/static.wms-dev.xyz/privkey.pem';
        }

        var options = {
            cert: fs.readFileSync(cert),
            key:  fs.readFileSync(privateKey),
        };
        console.log("CREEATE SERVER HTTPS");
        https.createServer(options, httpServer).listen(global.config.serv_static_httpport);
        http.createServer(httpServer).listen(global.config.serv_static_httpport + 1);
    }
    else {
        console.log("CREEATE SERVER HTTP");
        http.createServer(httpServer).listen(global.config.serv_static_httpport);
    }
    // var server         = http.createServer(httpServer);
    // server.listen(global.config.serv_static_httpport);
    console.log('==================', global.config.serv_static_httpport);
    // server.setTimeout(60000);

}


function httpServer(request, response) {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,x-csrf-token'); // If needed
    response.setHeader('Access-Control-Allow-Credentials', true); // If needed

    switch (request.method) {
        case 'GET':
            getRequest(request, response);
            break;
        default:
            postRequest(request, response);
            break;
    }
}

function postRequest(request, response) {
    response.setHeader('Content-Type', 'application/json'); // If needed
    var body = '';
    request.on('data', function (data) {
        body += data;
        // console.log(body);
        // if (body.length > 1e8)
        //     request.connection.destroy();
    });

    request.on('end', function () {
        // console.log('body - '+body);
        if (body) {

            var post = Object.assign({}, JSON.parse(body));

            if (post.hasOwnProperty('fromFile')) {
                fromFile(response, post);
                return;
            }

            var data = {
                method: 'Authentication/tokenverify',
                data:   {token: post.token},
            };
            console.log('SEND TO WORKER');
            global.Server.grpc.dataSend({data: JSON.stringify(data)}, function (err, res) {
                console.log('RESPONSE TO WORKER');
                if (err) {
                    console.log("!!!!!!!!!!!!!!!!!!!!!!222", err);
                    return returnErr(response);
                }
                var dataResp = {};
                console.log("!!!!!!!!!!!!!!!!!!!!!!res-", res);
                if (!res) return returnErr(response);

                try {
                    dataResp = JSON.parse(JSON.parse(res.data).data);
                    var uid  = null;
                    console.log('dataResp', dataResp);
                    console.log('dataResp2', typeof dataResp);
                    if (dataResp.hasOwnProperty('uid')) {
                        console.log('000', dataResp.uid);
                        uid = dataResp.uid;
                    }
                    else if (dataResp.hasOwnProperty('data')) {
                        uid = dataResp.data.uid;
                        console.log('000', dataResp.data.uid);
                    }
                    console.log("UID", uid);
                    if (uid) {
                        console.log('can upload');
                        uploadFile(uid, response, post);
                        return;
                    }

                    return returnErr(response, 'has error with user');
                } catch (ex) {
                    console.log('has error when upload');
                    return returnErr(response, 'has error when upload');
                }
            });
        }
        else {
            response.end();
        }
    });
}

function getRequest(request, response) {
    var filePath = '.' + request.url;
    if (filePath == './') {
        var pathDir = path.dirname(filePath) + '/';
    }
    var extname = path.extname(filePath.replace('', pathDir));
    console.log(pathDir);
    console.log(filePath);
    console.log(extname);
    var contentType = getContentType(extname);
    fs.readFile(filePath, function (error, content) {
        if (error) {
            if (error.code == 'ENOENT') {
                response.writeHead(404);
                response.end('Not found');
                response.end();
            }
            else {
                response.writeHead(500);
                response.end('Error: ' + error.code + ' ..\n');
                response.end();
            }
        }
        else {
            response.writeHead(200, {'Content-Type': contentType});
            response.end(content, 'utf-8');
        }
    });
}

function uploadFile(uid, response, data) {
    if (!data.hasOwnProperty('type')) return returnErr(response, 'invalid upload type');
    if (!data.hasOwnProperty('file')) return returnErr(response, 'invalid upload file');
    var request = {};
    console.log('check upload type', data.type);
    switch (data.type) {
        case 'photo':
            console.log('upload type photo');
            request = uploadPhoto(uid, response, data);
            break;
        case 'pdf':
            console.log('upload type PDF');
            request = uploadPdf(uid, response, data);
            break;
        default:
            console.log('upload type ALL');
            request = uploadPdf(uid, response, data);
            break;
    }
}

function uploadPdf(uid, response, data) {

    var dataString = data.file.content.split(',')[1];
    var path,
        userPath,
        fileType,
        filename,
        filePath,
        checkUserPath,
        urlPath,
        originalName,
        contentType,
        fileContents;

    urlPath     = '/storage/public/upload/docs/';
    path        = global.dirRoot + '/storage/public/upload/docs/';
    userPath    = path + uid + '/';
    fileType    = '.' + data.file.name.split('.').reverse()[0];
    contentType = data.file.mimeType;
    filename    = crypto.randomBytes(6).toString('hex') + (new Date()).valueOf().toString() + fileType;
    filePath    = userPath + filename;
    urlPath += uid + '/' + filename;
    console.log(userPath);
    console.log(filePath);
    checkUserPath = fs.existsSync(userPath);
    console.log('checkUserPath');
    console.log(checkUserPath);
    if (!checkUserPath) {
        fs.mkdirSync(userPath);

    }

    fileContents = dataString;//data.file.content.replace(/^data:application\/pdf;base64,/, "");

    originalName = data.file.name;

    fs.writeFileSync(filePath, fileContents, 'base64');

    var data = {
        url:        urlPath,
        mime_type:  contentType,
        path:       filePath,
        uid:        uid,
        name:       originalName,
        is_public:  1,
        sort_order: 1,
        file_size:  data.file.size,
        field_name: 'drop_file',
    };

    saveFile(data, response);
}

function uploadPhoto(uid, response, data) {
    var path,
        userPath,
        fileType,
        filename,
        filePath,
        checkUserPath,
        urlPath,
        originalName,
        contentType,
        fileContents;

    urlPath     = '/storage/public/upload/images/';
    path        = global.dirRoot + '/storage/public/upload/images/';
    userPath    = path + uid + '/';
    fileType    = '.png';
    contentType = data.file.mimeType;
    filename    = crypto.randomBytes(6).toString('hex') + (new Date()).valueOf().toString() + fileType;
    filePath    = userPath + filename;
    urlPath += uid + '/' + filename;
    console.log(userPath);
    console.log(filePath);

    checkUserPath = fs.existsSync(userPath);
    console.log('checkUserPath', checkUserPath);

    if (!checkUserPath) {
        fs.mkdirSync(userPath);
        fs.chmodSync(userPath, '777');
    }

    fileContents = data.file.content.replace(/^data:image\/png;base64,/, "");
    originalName = data.file.name;
    fs.writeFileSync(filePath, fileContents, 'base64');
    var data = {
        url:        urlPath,
        mime_type:  contentType,
        path:       filePath,
        name:       originalName,
        is_public:  1,
        sort_order: 1,
        file_size:  data.file.size,
        field_name: 'photo',
    };

    saveFile(data, response);
}

function getContentType(extname) {
    var contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.pdf':
            contentType = 'application/pdf';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            contentType = 'image/png';
            break;
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.jpeg':
            contentType = 'image/jpeg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
        default:
            contentType = 'application/' + extname.replace('.', '');
            break;
    }
    return contentType;
}

function saveFile(data, response) {
    var data = {
        method: 'Files/setFlie',
        data:   data,
    };

    global.Server.grpc.dataSend({data: JSON.stringify(data)}, function (err, res) {
        if (err) {
            console.log("!!!!!!!!!!!!!!!!!!!!!!222", err);
            return returnErr(response);
        }
        var dataResp = {};
        console.log("!!!!!!!!!!!!!!!!!!!!!!res-", res);
        if (!res) return returnErr(response);
        try {
            dataResp = JSON.parse(res.data);
            console.log(dataResp);
            var fileId;
            if (dataResp) {
                if (dataResp.hasOwnProperty('data')) dataResp = dataResp.data;
                if (dataResp.hasOwnProperty('_id')) fileId = dataResp._id;
            }
            return returnSuccess(response, {
                path: data.data.url,
                id:   fileId,
                name: data.data.name
            });
            console.log('has error with user');
        } catch (ex) {
            console.log('has error when upload');
            console.error(ex);
            return returnErr(response, 'has error when upload');
        }
    });
}

function returnErr(response, msg) {
    var msg       = msg ? msg : 'auth fail';
    var errorResp = {
        error:           true,
        'error_message': msg
    };
    response.end(JSON.stringify(errorResp));
}

function returnSuccess(res, msg) {
    var response = {
        success: true,
        data:    msg
    };
    console.log(response);
    res.end(JSON.stringify(response));
}

function download(uri, filename, response, callback) {
    request.head(uri, function (err, res, body) {
        if (err) {
            return returnErr(response, 'Can\'t download file');
        }
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
}

function fromFile(response, post) {
    var path,
        userPath,
        fileType,
        uid,
        filename,
        filePath,
        checkUserPath,
        urlPath,
        originalName,
        contentType;

    uid         = post.uid;
    urlPath     = '/storage/public/upload/images/';
    path        = global.dirRoot + '/storage/public/upload/images/';
    userPath    = path + uid + '/';
    fileType    = '.jpg';
    contentType = 'image/jpeg';
    filename    = crypto.randomBytes(6).toString('hex') + (new Date()).valueOf().toString() + fileType;
    filePath    = userPath + filename;
    urlPath += uid + '/' + filename;
    console.log(userPath);
    console.log(filePath);

    checkUserPath = fs.existsSync(userPath);
    console.log('checkUserPath');
    console.log(checkUserPath);

    if (!checkUserPath) fs.mkdirSync(userPath);

    download(post.fromFile, filePath, response, function () {
        var stats    = fs.statSync(filePath);
        originalName = filename;
        var data     = {
            url:        urlPath,
            mime_type:  contentType,
            path:       filePath,
            name:       originalName,
            is_public:  1,
            sort_order: 1,
            file_size:  stats.size,
            field_name: 'photo',
        };

        saveFile(data, response);
    })
}
