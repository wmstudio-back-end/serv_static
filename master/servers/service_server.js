var http = require("http");
var querystring = require('querystring');
var config = global.gfmaster.cacheModule.getConfig('SERVICE_SERVER_SETTINGS');

function echo_cross_domain(response){
    response.writeHead(200, {'Content-Type': 'application/xml; charset=UTF-8'});
    response.write('<?xml version="1.0"?>');
    response.write('<!DOCTYPE cross-domain-policy SYSTEM "http://www.adobe.com/xml/dtds/cross-domain-policy.dtd">');
    response.write('<cross-domain-policy>');
    response.write('<allow-access-from domain="*" to-ports="*"/>');
    response.write('</cross-domain-policy>');
    response.end();
}

module.exports = function (callback) {
    function onRequest(request, response){
        console.log('!!!!!!!')
        var res = function(error,data){
            response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
            if(error){
               data = error;
            }
            //console.log('=======================')
            //console.log(data);
            response.write(data);
            response.end();
        };
        if(request.url === "/crossdomain.xml"){
            echo_cross_domain(response);
        }
        var fullBody = '';
        request.on('data', function(chunk){
            fullBody += chunk.toString();
        });
        request.on('end', function(){
            var decodedBody = querystring.parse(fullBody);
            if(decodedBody.event && decodedBody.params){
                try {
                    decodedBody.params = JSON.parse(decodedBody.params);
                } catch(e) {
                    console.log("Service server : не удалось распарсить параметры в event:"+decodedBody.event);
                    return;
                }
            } else {
                return;
            }
            var masterRequest = global.gfmaster.router.controllers.master_requests;
            switch(decodedBody.event) {
                case 'getCach':
                var fs = require("fs"),
                    sys = require("sys");
                    fs.open("../../cacheServer.json", "w", 0644, function(err, file_handle) {
                    if (!err) {
                        
                       
                        fs.write(file_handle, JSON.stringify(global.gfmaster.cacheModule.getStaticCollections(), null, '\t'), null, 'ascii', function(err, written) {
                            if (!err) {
                             
                               
                            } else {
                              
                                console.log('Произошла ошибка при записи')
                            }
                        });
                    } else {
                       
                        console.log('Обработка ошибок при открытии')
                    }
                    });
                    //res(JSON.stringify(global.gfmaster.cacheModule.getStaticCollections()));
                    res('complite cache')
                    
                   
                break;
                case 'cache_update':
                    global.gfmaster.cacheModule.updateStaticTables(function(static_tables){
                        console.log("Мастер завершил обновление статичсеких таблиц :\n"+static_tables);
                    });
                    var count_process = Object.keys(global.gfmaster.cluster.workers).length;
                    var service_result = "";
                    //Уведомляем всех воркеров о перезагрузке статических таблиц
                    console.log("Процесс обновления статических таблиц - >\n-- Количество процессов : "+count_process);
                    masterRequest.request_all("service_controller","update_static_tables",{},function(result){
                        service_result += "\n"+result;
                        count_process -= 1;
                        console.log(result+"\n-- Ожидание процессов : "+count_process);
                        if(count_process == 0){
                            service_result +="\nПерезагрузка статических таблиц завершена";
                            res(null,service_result);
                            return;
                        }
                    });
                    return;
                    break;
                case 'delete_user':
                    /*if(!decodedBody.params.ppid){
                        res('Delete user - не указан ppid!',{});
                        return;
                    }
                    var worker = global.gfmaster.cacheModule.getWorkerPlayer(decodedBody.params.ppid);
                    if(worker){
                        masterRequest.request("service_controller","delete_user",{ppid:decodedBody.params.ppid},worker,
                            function(result){
                                res(null,result);
                                return;
                            });
                    } else {
                        findWorkerAndApply("service_controller","delete_user",{ppid:decodedBody.params.ppid},function(result){
                            res(null,result);
                            return;
                        });
                    }*/
                    return;
                    break;
                case 'add_stat':
                    /*if(!decodedBody.params.ppid && !decodedBody.params.stat && !decodedBody.params.count){
                        res('Add stat - некорректные входящие данные!',{});
                        return;
                    }
                    var worker = global.gfmaster.cacheModule.getWorkerPlayer(decodedBody.params.ppid);
                    if(!worker){
                        res('Для добавления стата игрок должен быть онлайн',{});
                        return;
                    }
                    masterRequest.request("service_controller","add_stat",{
                            ppid:decodedBody.params.ppid,
                            stat:decodedBody.params.stat,
                            count:decodedBody.params.count
                        },worker,
                        function(result){
                            res(null,result);
                            return;
                        });*/
                    return;
                    break;
                case 'gracefull_stop':
                    var count_process = Object.keys(global.gfmaster.cluster.workers).length;
                    var service_result = 'Инициирована останавка сервера, смотрите логи сервера....!';
                    masterRequest.request_all("service_controller","grace_full_stop",{},function(result){
                        count_process -= 1;
                        service_result += "\n"+result;
                        if(count_process == 0){
                            res(null,service_result);
                            return;
                        }
                    });
                    break;
                case 'kick_player':
                    /*if(!decodedBody.params.ppid){
                        res('Kick user - не указан ppid!',{});
                        return;
                    }
                    var worker = global.gfmaster.cacheModule.getWorkerPlayer(decodedBody.params.ppid);
                    if(worker){
                        masterRequest.request("service_controller","kick_player",{ppid:decodedBody.params.ppid},worker,
                            function(result){
                                res(null,result);
                                return;
                            });
                    } else {
                        res('Нету в памяти такого игрока!',{});
                        return;
                    }*/
                    return;
                    break;
                case 'notify':
                    /*if(global.gfmaster.router.controllers.hasOwnProperty(decodedBody.params.type)){
                        global.gfmaster.router.controllers[decodedBody.params.type].notify(decodedBody.params.message);
                        res("Рассылка ...",{});
                        return;
                    } else {
                        res("Не удалось разослать уведомления, контроллер не найден!",{});
                        return;
                    }*/
                    return;
                    break;
                case 'exec':
                       // var execSync  = execSync = require('exec-sync');
                        //var b = execSync('cat /proc/loadavg');
                        //console.log(b);
                    break;
                case 'get_online':
                    res(null,"Онлайн : "+global.gfmaster.cacheModule.getOnline());
                    return;
                    break;
            }
        });
    }
    if(config.ENABLED){
        http.createServer(onRequest).listen(config.PORT);
        callback(null,"Service server [OK]. Listening PORT : "+config.PORT);
    } else {
        callback(null,"Service server [DISABLED]");
    }
};

function findWorkerAndApply(controller,method,data,callback){
    global.gfmaster.router.controllers.master_requests.request(controller,method,data,getRandomWorker(),function(result){
            callback(result);
            return;
    });
}

function getRandomWorker(){
    var max = Object.keys(global.gfmaster.cluster.workers).length;
    return Math.round((Math.random()*(max-1)))+1;
}