var cookie = require('cookie');
var cookieParser = require('cookie-parser')
var OPTIONS = false
var https = require('https');
var http = require('http');
var fs = require('fs');
var path = require('path');
var qs = require('querystring');

function httpServer(request, response){
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    response.setHeader('Access-Control-Allow-Credentials', true); // If needed

    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e8)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            var data = {
                method:'worker/Authentication/identify',
                data : {token:post.token},
            };

            global.Server.client_controller.worker.dataSend({data:JSON.stringify(data)},function(err, res) {
                console.log("!!!!!!!!!!!!!!!!!!!!!!222",err)
                console.log("!!!!!!!!!!!!!!!!!!!!!!",res)
                var dataResp = {};
                console.log(res)
                dataResp = JSON.parse(res.data)
                if (dataResp.profile!=null){

                }
                response.end(JSON.stringify(dataResp))
            })
        });

    }
}


function socketRouter(data){
    http.createServer(httpServer).listen(8888);
}

module.exports.socketRouter = socketRouter