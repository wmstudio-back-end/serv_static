/**
 * @module config
 */
// Параметры соединения
exports.COMMON_CONNECTION = {
    serv_worker : 'localhost:50052',
    serv_static : 'localhost:40052',
    serv_graphql : 'localhost:30052',
    port:8182
};
