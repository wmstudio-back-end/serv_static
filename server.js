var Server = function(){}
Server.prototype.http = require('http');
Server.prototype.os = require('os');
Server.prototype.fs = require('fs');
Server.prototype.server_util = require('./app/modules/server_util')();
Server.prototype.worker = require('./master/modules/worker');
module.exports = Server