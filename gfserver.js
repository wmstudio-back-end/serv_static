global.argv = require('minimist')(process.argv.slice(2));
global.config = global.argv




console.info("[START SERVER]")

global.dirRoot = __dirname;
Server = require('./server');
global.Server = new Server();
global.Server.worker.initialize();

process.on('uncaughtException', function (err)
{
    console.log('<- UNCAUGHT ERROR START ------------------------------------------------------------------------------>');
    console.log(new Date());
    console.log(err.message);
    console.log(err.stack);
    console.log('<- UNCAUGHT ERROR END -------------------------------------------------------------------------------->');
});
